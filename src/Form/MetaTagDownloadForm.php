<?php

namespace Drupal\metatag_import_export_csv\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\metatag\MetatagManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that configures forms module settings.
 */
class MetaTagDownloadForm extends FormBase {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Drupal\metatag\MetatagManager definition.
   *
   * @var \Drupal\metatag\MetatagManager
   */
  protected $metatagManager;

  /**
   * Creates a new MetaTagDownloadForm instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\metatag\MetatagManagerInterface $metatag_manager
   *   The Metatag manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, MetatagManagerInterface $metatag_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->metatagManager = $metatag_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('metatag.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'metatag_import_export_download';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $entity_types = $this->getEntityTypesWithMetatags();
    if (empty($entity_types)) {
      $this->messenger()->addWarning($this->t("There are no metatag fields defined on any entity types."));

      return $form;
    }

    $wrapper_id = 'metatag_import_export_download-bundles';

    $form['entity_type'] = [
      '#title' => $this->t('Entity type'),
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => $entity_types,
      // Default to 'node', or the first entity type in the list if 'node' is
      // not in the available entity types.
      '#default_value' => isset($entity_types['node']) ? 'node' : array_keys($entity_types)[0],
      '#ajax' => [
        'callback' => '::getBundlesList',
        'event' => 'change',
        'wrapper' => $wrapper_id,
      ],
    ];

    if ($entity_type = $form_state->getValue('entity_type')) {
      $bundle_options = $this->getBundles($entity_type);
    }
    elseif (!empty($form['entity_type']['#default_value'])) {
      $bundle_options = $this->getBundles($form['entity_type']['#default_value']);
    }

    $form['bundles'] = [
      '#title' => $this->t('Bundle'),
      '#type' => 'select',
      '#multiple' => 'multiple',
      '#required' => TRUE,
      '#validated' => TRUE,
      '#options' => $bundle_options,
      '#prefix' => '<div id="' . $wrapper_id . '">',
      '#suffix' => '</div>',
    ];
    $form['tags'] = [
      '#title' => $this->t('Tags'),
      '#type' => 'select',
      '#required' => TRUE,
      '#multiple' => 'multiple',
      '#size' => 30,
      '#options' => $this->getTags(),
    ];
    $form['delimiter'] = [
      '#title' => $this->t('Delimiter'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#description' => $this->t('Enter the delimiter for CSV.'),
      '#default_value' => ',',
      '#maxlength' => 2,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#required' => TRUE,
      '#value' => $this->t('Download'),
    ];
    return $form;
  }

  /**
   * Returns entity types which have metatag fields.
   *
   * @return array
   *   An array whose keys are entity type IDs and values are the entity type
   *   labels.
   */
  public function getEntityTypesWithMetatags() {
    $entity_types = [];

    $entity_type_manager = $this->entityTypeManager;
    foreach ($entity_type_manager->getDefinitions() as $entity_type_id => $entity_type) {
      if ($entity_type->getGroup() == 'content' && $this->hasBundlesWithMetatags($entity_type_id)) {
        $entity_types[$entity_type_id] = $entity_type->getLabel();
      }
    }
    natcasesort($entity_types);
    return $entity_types;
  }

  /**
   * Checks if entity type has at least one bundle with metatag field.
   *
   * @param string $entity_type
   *   The entity type ID.
   *
   * @return bool
   *   TRUE if entity type has bundles with metatags field, FALSE otherwise.
   */
  public function hasBundlesWithMetatags(string $entity_type): bool {
    $fields_map = \Drupal::service('entity_field.manager')->getFieldMap();
    $entity_fields = $fields_map[$entity_type];

    foreach ($entity_fields as $field) {
      if ($field["type"] == 'metatag') {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Ajax callback for entity type field.
   */
  public function getBundlesList($form, FormStateInterface &$form_state) {
    return $form['bundles'];
  }

  /**
   * Returns bundles.
   */
  public function getBundles($entity_type) {
    $fields_map = \Drupal::service('entity_field.manager')->getFieldMap();
    $entity_fields = $fields_map[$entity_type];

    $bundles = [];

    foreach ($entity_fields as $field) {
      if ($field["type"] == 'metatag') {
        $bundles = $field['bundles'];
      }
    }

    if (!empty($bundles)) {
      $bundleList = $this->entityTypeBundleInfo->getBundleInfo($entity_type);
      foreach ($bundleList as $keys => $values) {
        if (isset($bundles[$keys])) {
          $bundles[$keys] = (string) $values['label'];
        }
      }
    }

    return $bundles;
  }

  /**
   * Returns tags supported by metatag module.
   */
  public function getTags() {
    $tagList = [];
    $metatag_manager = $this->metatagManager;
    $tagGroupList = $metatag_manager->sortedGroupsWithTags();
    foreach ($tagGroupList as $keys => $values) {
      $tagList[$keys] = $values['tags'];
      foreach ($values['tags'] as $tkeys => $tvalues) {
        $tagList[$keys][$tkeys] = $tvalues['label'];
      }
    }
    return $tagList;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity_type = $form_state->getValue('entity_type');
    $bundles = $form_state->getValue('bundles');
    $tags = $form_state->getValue('tags');
    $delimiter = $form_state->getValue('delimiter');
    $this->metatagImportExportCsvDownload($entity_type, $bundles, $tags, $delimiter);
  }

  /**
   * Gets list of all entity and  passes to batch.
   */
  public function metatagImportExportCsvDownload($entity_type, $bundles, $tags, $delimiter) {
    $query = \Drupal::entityQuery($entity_type);
    $query->accessCheck(FALSE);

    $entity_type_definition = $this->entityTypeManager->getDefinition($entity_type);
    if ($bundle_key = $entity_type_definition->getKey('bundle')) {
      $query->condition($bundle_key, $bundles, 'IN');
    }

    // @todo Generalise this.
    switch ($entity_type) {
      case 'node':
        $query->sort('created', 'ASC');
        break;
    }

    $nidlist = $query->execute();
    $nidlist = array_values($nidlist);
    $total_count = count($nidlist);
    $operations = [];
    for ($i = 0; $i < $total_count; $i++) {
      $operations[] = [
        '\Drupal\metatag_import_export_csv\MetatagExport::generateExportCsv',
        [
          $nidlist[$i],
          $entity_type,
          $tags,
          $delimiter,
        ],
      ];
    }
    $batch = [
      'operations' => $operations,
      'finished' => '\Drupal\metatag_import_export_csv\MetatagExport::downloadCsv',
      'title' => $this->t('Metatags export'),
      'init_message' => $this->t('Export process is starting.'),
      'progress_message' => $this->t('Processed @current out of @total.'),
      'error_message' => $this->t('Batch has encountered an error.'),
    ];
    batch_set($batch);
  }

}
