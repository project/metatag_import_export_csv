<?php

namespace Drupal\metatag_import_export_csv\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Form  for uploading csv.
 */
class MetaTagUploadForm extends FormBase {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Creates a new MetaTagUploadForm instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'metatag_import_export_upload';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['upload_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Upload file'),
      '#required' => TRUE,
      '#description' => $this->t('Allowed extensions: csv'),
      '#upload_location' => 'public://metatag_import_export_csv/',
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
      ],
    ];
    $form['delimiter'] = [
      '#title' => $this->t('Delimiter'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#description' => $this->t('Enter the delimiter for CSV.'),
      '#default_value' => ',',
      '#maxlength' => 2,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#required' => TRUE,
      '#value' => $this->t('Import'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $fid = $form_state->getValue('upload_file')[0];
    $file = $this->entityTypeManager->getStorage('file')->load($fid);
    $delimiter = $form_state->getValue('delimiter');

    // Check if it is possible to open the file.
    $handle = fopen($file->getFileUri(), 'r');

    if (!$handle) {
      $form_state->setErrorByName('upload_file', $this->t("The CSV file can't be opened."));
    }
    else {
      $headers = trim(fgets($handle));
      // Close the CSV file, as the batch setup needs to open it from the top.
      fclose($handle);

      // Check if file used correct delimiter.
      if (!str_contains($headers, $delimiter)) {
        $form_state->setErrorByName('upload_file', $this->t('The CSV file used wrong delimiter.'));
      }
      else {
        $headers_array = explode($delimiter, $headers);

        // Check if file contains required columns.
        if (!in_array('path_alias', $headers_array)) {
          if (!in_array('entity_type', $headers_array)) {
            $form_state->setErrorByName('upload_file', $this->t("The CSV must have an 'entity_type' column if it does not have a 'path_alias' column."));
          }

          if (!in_array('entity_id', $headers_array)) {
            $form_state->setErrorByName('upload_file', $this->t("The CSV must have an 'entity_id' column if it does not have a 'path_alias' column."));
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $fid = $form_state->getValue('upload_file')[0];
    $file = $this->entityTypeManager->getStorage('file')->load($fid);
    $file->setTemporary();
    $file->save();
    $delimiter = $form_state->getValue('delimiter');

    $this->metatagImportExportCsvUpload($file->getFileUri(), $delimiter);
  }

  /**
   * Reads each line from csv and passes it to batch.
   */
  public function metatagImportExportCsvUpload($filepath, $delimiter) {
    $handle = fopen($filepath, 'r');
    $headers = fgetcsv($handle, NULL, $delimiter);
    fclose($handle);

    $operations = [];
    $operations[] = [
      '\Drupal\metatag_import_export_csv\MetatagImport::importCsvBatchOperation',
      [
        $headers,
        $filepath,
        $delimiter,
      ],
    ];

    $batch = [
      'operations' => $operations,
      'finished' => '\Drupal\metatag_import_export_csv\MetatagImport::importFinish',
      'title' => $this->t('Metatags import'),
      'init_message' => $this->t('Import process is starting.'),
      'progress_message' => $this->t('Processed @current out of @total.'),
      'error_message' => $this->t('Batch has encountered an error.'),
    ];
    batch_set($batch);
  }

}
