<?php

namespace Drupal\metatag_import_export_csv;

use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Link;

/**
 * Class for batch process for metatag export.
 */
class MetatagExport {

  /**
   * Batch Callback to generate csv.
   */
  public static function generateExportCsv($id, $entity_type, $tags, $delimeter, &$context = []) {
    $entity = [];
    $entity = \Drupal::service('entity_type.manager')->getStorage($entity_type)->load($id);
    if (!empty($entity)) {
      // Metatag v1 function.
      if (function_exists('metatag_generate_entity_metatags')) {
        $entityTags = metatag_generate_entity_metatags($entity);
      }
      // Metatag v2 function.
      else {
        $entityTags = metatag_generate_entity_all_tags($entity);
      }
      $definitions = $entity->getFieldDefinitions();
      $tagsList = [];
      $tagsList['entity_id'] = $id;
      $tagsList['entity_title'] = $entity->label();
      $tagsList['entity_bundle'] = $entity->bundle();
      $tagsList['entity_type'] = $entity_type;
      $tagsList['field_machine_name'] = '';
      foreach ($definitions as $definition) {
        $field_type = $definition->getType();
        if ($field_type == 'metatag') {
          $tagsList['field_machine_name'] = $definition->getName();
        }
      }
      $tagsList['alias'] = \Drupal::service('path_alias.manager')->getAliasByPath('/' . $entity_type . '/' . $id);
      foreach ($tags as $keys => $values) {
        if (!empty($entityTags[$keys])) {
          if (isset($entityTags[$keys]['#attributes']['content'])) {
            $tagsList[$keys] = $entityTags[$keys]['#attributes']['content'];
          }
          elseif (isset($entityTags[$keys]['#attributes']['href'])) {
            $tagsList[$keys] = $entityTags[$keys]['#attributes']['href'];
          }
          else {
            $tagsList[$keys] = '';
          }
        }
        else {
          $tagsList[$keys] = '';
        }
      }
      $context['results']['rows'][] = $tagsList;
      $context['results']['delimeter'] = $delimeter;
      $context['results']['tags'] = $tags;
      $context['results']['entity_type'] = $entity_type;
    }
  }

  /**
   * Batch 'finished' callback used to download csv.
   */
  public static function downloadCsv($success, $results, $operations) {
    $messenger = \Drupal::messenger();
    if ($success && isset($results['rows'])) {
      $delimeter = $results['delimeter'];
      $filePath = 'public://metatag_import_export_csv/';
      $fileName = 'metatag_import_export_csv__' . $results['entity_type'] . '.csv';
      $fileUrl = $filePath . $fileName;
      $response = new Response();
      $response->headers->set('Content-Type', 'text/csv');
      $response->headers->set('Content-Disposition', 'attachment;filename=' . $fileName);
      $header = $results['tags'];
      array_unshift($header, 'entity_id', 'entity_title', 'entity_bundle', 'entity_type', 'field_machine_name', 'alias');
      if (!(\Drupal::service('file_system')->prepareDirectory($filePath, FileSystemInterface::CREATE_DIRECTORY))) {
        $messenger->addMessage(t('Unable to create directory in file system for error File. Check permissions and try again.'), 'error');
        return;
      }
      $file = \Drupal::service('file_system')->saveData('', $fileUrl, FileSystemInterface::EXISTS_REPLACE);
      if (!($file)) {
        $messenger->addMessage(t('Unable to write to file system. Check permissions and try again.'), 'error');
        return;
      }
      $fp = fopen($file, 'w');
      if (!$fp) {
        $messenger->addMessage(t('Unable open file for writing. Check permissions and try again.'), 'error');
        return;
      }
      fputcsv($fp, $header, $delimeter);
      foreach ($results['rows'] as $row) {
        fputcsv($fp, $row, $delimeter);
      }
      fclose($fp);
      $link = Link::fromTextAndUrl($fileName, \Drupal::service('file_url_generator')->generate($fileUrl));
      $messenger->addMessage(t('Download Csv : @args', [
        '@args' => $link->toString(),
      ]));
    }
    elseif ($success) {
      // The batch ran OK but there were no entities to process.
      $messenger->addWarning("No entities were found. No CSV file has been created.");
    }
    else {
      $error_operation = reset($operations);
      $messenger->addMessage(t('An error occurred while processing @operation with arguments : @args', [
        '@operation' => $error_operation[0],
        '@args' => print_r($error_operation[0], TRUE),
      ]), 'error');
    }
  }

}
