<?php

namespace Drupal\metatag_import_export_csv;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;

/**
 * Class for batch process for metatag import.
 */
class MetatagImport {

  /**
   * Batch callback for parsing the CSV.
   *
   * @param array $headers
   *   The header row of the CSV file. This must contain an 'entity_type' and
   *   'entity_id' column.
   * @param mixed $filepath
   *   The filepath to the uploaded CSV file.
   * @param string $delimiter
   *   The delimiter of CSV file.
   * @param array $context
   *   The batch context.
   */
  public static function importCsvBatchOperation(array $headers, mixed $filepath, string $delimiter, array &$context = []) {
    // Initial setup on the first operation.
    if (empty($context['sandbox'])) {
      $context['sandbox']['handle'] = NULL;
      $context['sandbox']['pointer_position'] = 0;
      $context['sandbox']['csv_line'] = 0;

      $context['results']['success'] = [];
      $context['results']['error'] = [];
    }

    if (!is_resource($context['sandbox']['handle'])) {
      $context['sandbox']['handle'] = fopen($filepath, 'r');
    }

    // If we've reached the end of the file, we're done.
    if (feof($context['sandbox']['handle'])) {
      $context['finished'] = 1;

      return;
    }

    if ($context['sandbox']['pointer_position'] == 0) {
      // On the first operation, discard the CSV header row.
      fgetcsv($context['sandbox']['handle'], NULL, $delimiter);
      $context['sandbox']['csv_line']++;
    }
    else {
      // On subsequent operations, move the pointer to where it was for the last
      // operation.
      fseek($context['sandbox']['handle'], $context['sandbox']['pointer_position']);
    }

    // Get the data, and update our stored pointer.
    $data = fgetcsv($context['sandbox']['handle'], NULL, $delimiter);
    $context['sandbox']['csv_line']++;
    $context['sandbox']['pointer_position'] = ftell($context['sandbox']['handle']);

    $field_name_index = array_search('field_machine_name', $headers);
    if ((FALSE === $field_name_index) || empty($data[$field_name_index])) {
      $context['results']['error'][] = t('Unable to import line @line of the CSV file, there is no "Metatag" field for the values to be saved on.', [
        '@line' => $context['sandbox']['csv_line'],
      ]);
    }

    else {
      try {
        $entity = static::importCsvRow($headers, $data);

        $context['results']['success'][$entity->getEntityTypeId()][] = $entity->id();
      }
      catch (\Exception $e) {
        $context['results']['error'][] = t("Unable to import line @line of the CSV. Error was: '@message'.", [
          '@line' => $context['sandbox']['csv_line'],
          '@message' => $e->getMessage(),
        ]);
      }
    }

    // We're not finished until we reach the end of the CSV file.
    $context['finished'] = 0;
  }

  /**
   * Imports a single row of data from the CSV.
   *
   * @param array $headers
   *   The header row of the CSV file.
   * @param array $data
   *   A data row of the CSV file.
   *
   * @return object
   *   The entity which has been updated by the CSV row.
   *
   * @throws \Exception
   *   Throws an exception if there was a problem finding or saving the entity.
   */
  public static function importCsvRow(array $headers, array $data) {
    $entity_type_index = array_search('entity_type', $headers);
    $entity_id_index = array_search('entity_id', $headers);
    $language_index = array_search('language', $headers);
    $path_alias_index = array_search('path_alias', $headers);

    $entity = [];

    if ($entity_type_index !== FALSE && $entity_id_index !== FALSE) {
      $entity_type = trim($data[$entity_type_index]);
      $entity_id = trim($data[$entity_id_index]);
    }

    if ($path_alias_index !== FALSE) {
      $path_alias = trim($data[$path_alias_index]);
    }

    if (!((!empty($entity_type) && !empty($entity_id)) || !empty($path_alias))) {
      throw new \Exception('Either both of entity_id and entity_type or path_alias must be specified.');
    }

    if ($language_index !== FALSE) {
      $langcode = trim($data[$language_index]);
    }

    // Resolve the path alias if that is being used.
    if (!empty($path_alias)) {
      $path = \Drupal::service('path_alias.manager')->getPathByAlias($path_alias, $langcode ?? NULL);
      $url = Url::fromUri('internal:' . $path);

      // Check the URL found an internal route for the path, as otherwise the
      // exception thrown by getRouteParameters() stating that the URL is
      // external is not clear in the user's context.
      if (!$url->isRouted()) {
        throw new \Exception(t("The path alias '@alias' does not correspond to a route.", [
          '@alias' => $path_alias,
        ]));
      }

      $route_parameters = $url->getRouteParameters();
      $entity_type = key($route_parameters);

      if (!\Drupal::service('entity_type.manager')->hasDefinition($entity_type)) {
        throw new \Exception(t("The path alias '@alias' does not correspond to an entity route.", [
          '@alias' => $path_alias,
        ]));
      }

      $entity_id = $route_parameters[$entity_type];
    }

    if (!\Drupal::service('entity_type.manager')->hasDefinition($entity_type)) {
      throw new \Exception(t("The '@entity-type' entity type does not exist.", [
        '@entity-type' => $entity_type,
      ]));
    }

    // Load the entity.
    $entity = \Drupal::service('entity_type.manager')->getStorage($entity_type)->load($entity_id);

    if (!$entity) {
      throw new \Exception(t('No @entity-type with ID @entity-id was found.', [
        '@entity-type' => $entity_type,
        '@entity-id' => $entity_id,
      ]));
    }

    // Get the translation if the language is specified in the CSV data.
    if (!empty($langcode)) {
      if (!$entity->hasTranslation($langcode)) {
        throw new \Exception(t('The @entity-type with ID @entity-id has no @language translation.', [
          '@entity-type' => $entity_type,
          '@entity-id' => $entity_id,
          '@language' => $langcode,
        ]));
      }

      $entity = $entity->getTranslation($langcode);
    }

    // Metatag v1 function.
    if (function_exists('metatag_generate_entity_metatags')) {
      $output_tags = metatag_generate_entity_metatags($entity);
    }
    // Metatag v2 function.
    else {
      $output_tags = metatag_generate_entity_all_tags($entity);
    }

    $tags = [];
    foreach ($headers as $keys => $values) {
      if (trim($data[$keys]) == '_blank') {
        $tags[$values] = '';
      }
      else {
        // Only assign the values if they are different to what would be
        // generated for this entity.
        $tags[$values] = $data[$keys];
      }
    }

    // Remove non meta tags fields.
    $metatag_machine_name = $tags['field_machine_name'];
    unset($tags['entity_id'], $tags['entity_title'], $tags['entity_type'], $tags['alias'], $tags['field_machine_name']);

    // Remove values that are the same as what would be generated.
    foreach ($output_tags as $tag_name => $output_tag) {
      // Don't bother doing anything if this value isn't in the new data.
      if (!isset($tags[$tag_name])) {
        continue;
      }

      // Work out what the output value would be.
      $output_value = '';
      if (isset($output_tag['#attributes']['content'])) {
        $output_value = $output_tag['#attributes']['content'];
      }
      elseif (isset($entityTags[$keys]['#attributes']['href'])) {
        $output_value = $output_tag['#attributes']['href'];
      }

      // If the output value is equal to the value from the CSV file, skip this
      // value.
      if ($output_value == $tags[$tag_name]) {
        unset($tags[$tag_name]);
      }
    }

    if (!empty($tags)) {
      try {
        // This function was added in v8.x-1.23 and v2.0.0, so if it is
        // available then use it, otherwise fall back to the older v1 method.
        if (function_exists('metatag_data_encode')) {
          $tags = metatag_data_encode($tags);
        }
        else {
          $tags = serialize($tags);
        }
        $entity->set($metatag_machine_name, $tags);
        $entity->save();
      }
      catch (\Exception $e) {
        throw new \Exception(t("Error saving entity @entity-type @entity-id: '@message'.", [
          '@entity-type' => $entity_type,
          '@entity-id' => $entity_id,
          '@message' => $e->getMessage(),
        ]));
      }
    }

    return $entity;
  }

  /**
   * Batch finished callback for import.
   */
  public static function importFinish($success, $results, $operations) {
    $messenger = \Drupal::messenger();

    if ($success) {
      $message_entity_summary = [];

      foreach ($results['success'] as $entity_type => $entity_ids) {
        $entity_type_definition = \Drupal::service('entity_type.manager')->getDefinition($entity_type);
        $message_entity_summary[] = \Drupal::service('string_translation')->formatPlural(count($entity_ids), '1 ' . $entity_type_definition->getSingularLabel(), '@count ' . $entity_type_definition->getPluralLabel());
      }

      if ($message_entity_summary) {
        $messenger->addMessage(t('Successfully updated entities: @summary.', [
          '@summary' => implode(', ', $message_entity_summary),
        ]));
      }

      foreach ($results['error'] as $error_message) {
        $messenger->addError($error_message);
      }
    }
    else {
      $error_operation = reset($operations);
      $messenger->addMessage(t('An error occurred while processing @operation with arguments : @args', [
        '@operation' => $error_operation[0],
        '@args' => print_r($error_operation[0], TRUE),
      ]), 'error');
    }
  }

}
