# Metatag Import Export CSV

This module provides a way to export a CSV file of meta tags provided by the
Metatag module for entity values (content, taxonomy terms, etc), and allows
uploading a file to modify these values. Supports all meta tag values which are
provided by Metatag module.

This module has two separate forms for import and export.

* Export - you can select the entity type (node, user, taxonomy), bundle type
  (content type, vocabulary), tags and delimeter, a CSV file will be downloaded.
* Import - Upload the CSV file in the format given in sample file and meta tags
  will imported with success and error messages.

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/metatag_import_export_csv).


## File format for CSV file

The import CSV file may have the following fields, some combinations of which
mandatory:

* `entity_type` - The type of entity. Required unless 'path_alias' is populated.
* `entity_id` - The ID of the entity. Required unless 'path_alias' is populated.
* `path_alias` - The path alias of the entity including the initial '/'.
  Required if `entity_type` and `entity_id` are empty.
* `field_machine_name` - The machine name of meta tag field on the entity. This
  field is mandatory.
* `language` - The language of the entity translation. Optional.

Other fields are field name of tags.

* Leave a cell empty if you don't want to change the value of that field.
* Type `_blank` if you want to set a value of any field empty.


## Requirements

This module requires the following modules:

 * [Metatag](https://www.drupal.org/project/metatag)
 * [Token](https://www.drupal.org/project/token)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Configure user permissions in Administration >> People >> Permissions:
  * Metatag Import Export CSV download
  * Metatag Import Export CSV upload
2. Configuration >> Search and metadata >> Metatag >> Export.

  Export a CSV of meta tags for given entity types and specific meta tags.
3. Configuration >> Search and metadata >> Metatag >> Import.

  Import a CSV file to update the meta tags for the included entities.


## Maintainers

- Aayush Mittal - [aayush23](https://www.drupal.org/u/aayush23)
- [Damien McKenna](https://www.drupal.org/u/damienmckenna)
- Joachim Noreiko - [joachim](https://www.drupal.org/u/joachim)
- Namit Garg - [namit.garg](https://www.drupal.org/u/namitgarg)
