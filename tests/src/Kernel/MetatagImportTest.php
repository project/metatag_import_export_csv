<?php

namespace Drupal\Tests\metatag_import_export_csv\Kernel;

use Drupal\Component\Serialization\Json;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\metatag_import_export_csv\MetatagImport;

/**
 * Tests importing CSV data.
 *
 * @group metatag_import_export_csv
 */
class MetatagImportTest extends EntityKernelTestBase {

  /**
   * The modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'user',
    'token',
    'metatag',
    'metatag_import_export_csv',
    'language',
    'content_translation',
    'path',
    'path_alias',
    'node',
  ];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->entityTypeManager = $this->container->get('entity_type.manager');

    $this->installSchema('node', 'node_access');
    $this->installEntitySchema('node');
    $this->installEntitySchema('path_alias');

    $this->installConfig(['language']);
    ConfigurableLanguage::createFromLangcode('fr')->save();

    // Setup an anonymous user, so URL operations work.
    // See https://www.drupal.org/project/drupal/issues/3056234.
    $this->entityTypeManager->getStorage('user')->create([
      'name' => '',
      'uid' => 0,
    ])->save();

    // Create the node type.
    $node_type_storage = $this->entityTypeManager->getStorage('node_type');
    $type = $node_type_storage->create([
      'type' => 'page',
      'name' => 'Page',
    ]);
    $type->save();

    // Create the metatags field.
    $field_name = 'field_metatags';
    $this->entityTypeManager->getStorage('field_storage_config')->create([
      'field_name' => $field_name,
      'entity_type' => 'node',
      'type' => 'metatag',
    ])->save();

    $this->entityTypeManager->getStorage('field_config')->create([
      'field_name' => $field_name,
      'entity_type' => 'node',
      'bundle' => 'page',
    ])->save();
  }

  /**
   * Test entities are updated from the CSV data.
   */
  public function testEntityUpdate() {
    $node_storage = $this->entityTypeManager->getStorage('node');

    $nodes = [];

    // Simple node.
    $node = $node_storage->create([
      'type' => 'page',
      'title' => 'Title 1',
    ]);
    $node->save();
    $nodes[1] = $node;

    // Node with an existing value.
    $node = $node_storage->create([
      'type' => 'page',
      'title' => 'Title 2',
      'field_metatags' => $this->encodeData(['abstract' => 'old value']),
    ]);
    $node->save();
    $nodes[2] = $node;

    // Translated node, only changing the original language.
    $node = $node_storage->create([
      'type' => 'page',
      'title' => 'Title 3',
      'field_metatags' => $this->encodeData(['abstract' => 'old value']),
    ]);
    $node->save();
    $nodes[3] = $node;
    $translation = $node->addTranslation('fr', $node->toArray());
    $translation->field_metatags = $this->encodeData(['abstract' => 'ancienne valeur']);
    $translation->save();

    // Translated node, changing only the translation.
    $node = $node_storage->create([
      'type' => 'page',
      'title' => 'Title 4',
      'field_metatags' => $this->encodeData(['abstract' => 'old value']),
    ]);
    $node->save();
    $nodes[4] = $node;
    $translation = $node->addTranslation('fr', $node->toArray());
    $translation->field_metatags = $this->encodeData(['abstract' => 'ancienne valeur']);
    $translation->save();

    // Translated node, changing both languages.
    $node = $node_storage->create([
      'type' => 'page',
      'title' => 'Title 5',
      'field_metatags' => $this->encodeData(['abstract' => 'old value']),
    ]);
    $node->save();
    $nodes[5] = $node;
    $translation = $node->addTranslation('fr', $node->toArray());
    $translation->field_metatags = $this->encodeData(['abstract' => 'ancienne valeur']);
    $translation->save();

    // Node with a path alias.
    $node = $node_storage->create([
      'type' => 'page',
      'title' => 'Title 6',
      'path' => ['alias' => '/foo'],
    ]);
    $node->save();
    $nodes[6] = $node;

    // Node with translated path alias.
    $node = $node_storage->create([
      'type' => 'page',
      'title' => 'Title 7',
      'path' => ['alias' => '/bar'],
    ]);
    $node->save();
    $nodes[7] = $node;
    $translation = $node->addTranslation('fr', $node->toArray());
    $translation->path->alias = '/toto';
    $translation->save();

    // CSV data to import.
    $headers = [
      'entity_type',
      'entity_id',
      'path_alias',
      'language',
      'field_machine_name',
      'abstract',
    ];
    $data = [
      ['node', $nodes[1]->id(), '', '', 'field_metatags', 'new value one'],
      ['node', $nodes[2]->id(), '', '', 'field_metatags', 'new value two'],
      ['node', $nodes[3]->id(), '', '', 'field_metatags', 'new value three'],
      [
        'node',
        $nodes[4]->id(),
        '',
        'fr',
        'field_metatags',
        'nouvelle valeur quatre',
      ],
      ['node', $nodes[5]->id(), '', 'en', 'field_metatags', 'new value five'],
      [
        'node',
        $nodes[5]->id(),
        '',
        'fr',
        'field_metatags',
        'nouvelle valeur cinq',
      ],
      ['', '', '/foo', '', 'field_metatags', 'new value six'],
      ['', '', '/bar', 'en', 'field_metatags', 'new value seven'],
      ['', '', '/toto', 'fr', 'field_metatags', 'nouvelle valeur sept'],
    ];

    foreach ($data as $row) {
      MetatagImport::importCsvRow($headers, $row);
    }

    foreach ($nodes as $index => $node) {
      $nodes[$index] = $this->reloadEntity($node);
    }

    $metatag_data = $this->decodeData($nodes[1]->field_metatags->value);
    $this->assertEquals('new value one', $metatag_data['abstract']);

    $metatag_data = $this->decodeData($nodes[2]->field_metatags->value);
    $this->assertEquals('new value two', $metatag_data['abstract']);

    // Original is affected.
    $metatag_data = $this->decodeData($nodes[3]->field_metatags->value);
    $this->assertEquals('new value three', $metatag_data['abstract']);
    // Translation is not affected.
    $metatag_data = $this->decodeData($nodes[3]->getTranslation('fr')->field_metatags->value);
    $this->assertEquals('ancienne valeur', $metatag_data['abstract']);

    // Original is not affected.
    $metatag_data = $this->decodeData($nodes[4]->field_metatags->value);
    $this->assertEquals('old value', $metatag_data['abstract']);
    // Translation is updated.
    $metatag_data = $this->decodeData($nodes[4]->getTranslation('fr')->field_metatags->value);
    $this->assertEquals('nouvelle valeur quatre', $metatag_data['abstract']);

    // Both translations are updated.
    $metatag_data = $this->decodeData($nodes[5]->field_metatags->value);
    $this->assertEquals('new value five', $metatag_data['abstract']);
    $metatag_data = $this->decodeData($nodes[5]->getTranslation('fr')->field_metatags->value);
    $this->assertEquals('nouvelle valeur cinq', $metatag_data['abstract']);

    $metatag_data = $this->decodeData($nodes[6]->field_metatags->value);
    $this->assertEquals('new value six', $metatag_data['abstract']);

    // Both translations are updated.
    $metatag_data = $this->decodeData($nodes[7]->field_metatags->value);
    $this->assertEquals('new value seven', $metatag_data['abstract']);
    $metatag_data = $this->decodeData($nodes[7]->getTranslation('fr')->field_metatags->value);
    $this->assertEquals('nouvelle valeur sept', $metatag_data['abstract']);

    // Check other field values are not changed.
    foreach ($nodes as $index => $node) {
      $this->assertEquals("Title {$index}", $node->title->value);
    }
  }

  /**
   * Tests handling of badly-formed CSV data.
   *
   * @param array $headers
   *   The row of CSV headers.
   * @param array $data
   *   A row of mocked CSV data.
   *
   * @dataProvider providerBadData
   */
  public function testBadData(array $headers, array $data) {
    $node = $this->entityTypeManager->getStorage('node')->create([
      'type' => 'page',
      'title' => 'Title',
    ]);
    $node->save();

    $this->expectException(\Exception::class);

    MetatagImport::importCsvRow($headers, $data);
  }

  /**
   * Data provider for testBadData().
   */
  public function providerBadData() {
    return [
      'missing entity type' => [
        [
          'entity_type',
          'entity_id',
          'path_alias',
          'field_machine_name',
          'abstract',
        ],
        ['', 1, '', 'field_metatags', 'tag value'],
      ],
      'missing entity ID' => [
        [
          'entity_type',
          'entity_id',
          'path_alias',
          'field_machine_name',
          'abstract',
        ],
        ['node', '', '', 'field_metatags', 'tag value'],
      ],
      'missing any entity details' => [
        [
          'entity_type',
          'entity_id',
          'path_alias',
          'field_machine_name',
          'abstract',
        ],
        ['', '', '', 'field_metatags', 'tag value'],
      ],
      'bad entity type' => [
        ['entity_type', 'entity_id', 'field_machine_name', 'abstract'],
        ['cake', 1, 'field_metatags', 'tag value'],
      ],
      'bad entity id' => [
        ['entity_type', 'entity_id', 'field_machine_name', 'abstract'],
        ['node', 47, 'field_metatags', 'tag value'],
      ],
      'bad language' => [
        [
          'entity_type',
          'entity_id',
          'language',
          'field_machine_name',
          'abstract',
        ],
        ['node', 47, 'klingon', 'field_metatags', 'tag value'],
      ],
      'missing translation' => [
        [
          'entity_type',
          'entity_id',
          'language',
          'field_machine_name',
          'abstract',
        ],
        ['node', 47, 'fr', 'field_metatags', 'tag value'],
      ],
      'bad field' => [
        [
          'entity_type',
          'entity_id',
          'language',
          'field_machine_name',
          'abstract',
        ],
        ['node', 1, '', 'bad_field', 'tag value'],
      ],
      'bad path alias' => [
        ['path_alias', 'field_machine_name', 'abstract'],
        ['/bad-alias', 'field_metatags', 'tag value'],
      ],
    ];
  }

  /**
   * Encode the data for Metatag.
   *
   * Using this function allows supporting both Metatag v1 and v2.
   *
   * @param array $data
   *   The meta tag data array to encode.
   *
   * @return string
   *   The encoded meta data.
   */
  protected function encodeData(array $data = []): string {
    // This function was added in v8.x-1.23 and v2.0.0, so if it is available
    // then use it, otherwise fall back to the older v1 method.
    if (function_exists('metatag_data_encode')) {
      $data = metatag_data_encode($data);
    }
    else {
      $data = serialize($data);
    }
    return $data;
  }

  /**
   * Decode the data for Metatag.
   *
   * Using this function allows supporting both Metatag v1 and v2.
   *
   * @param string $data
   *   The encoded meta data.
   *
   * @return array
   *   The meta tag data array after decoding.
   */
  protected function decodeData($data = ''): array {
    // This function was added in v8.x-1.23 and v2.0.0, so if it is available
    // then use it, otherwise fall back to the older v1 method.
    if (function_exists('metatag_data_decode')) {
      $data = metatag_data_decode($data);
    }
    else {
      $data = unserialize($data, ['allowed_classes' => FALSE]);
    }
    return $data;
  }

}
